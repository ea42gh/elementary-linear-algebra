---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'Arnoldi_Ritz_eigenvalues.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. The Arnoldi Algorithm
## 1.1 Krylov Subspaces $\mathbf{\mathcal{K}_k}$
## 1.2 Orthonormal Basis for  $\mathbf{\mathcal{K}_k}$
## 1.3 Derivation of the Algorithm
## 1.4 The Arnoldi Algorithm
## 1.5 Relationship between $\mathbf{H_k}$ and $\mathbf{QR}$
## 1.6 Symmetric Matrix: the Lanczos Algorithm
# 2. Ritz Eigenvalues
## 2.1 Construct an Example
