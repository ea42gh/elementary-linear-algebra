---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'GradientDescentExample.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Objective Function
# 2. Gradient Descent
# 3. Improved Algorithm
## 3.1. Exact Line Search
## 3.2. Accelerated Descent: Momentum
