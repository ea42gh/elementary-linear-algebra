---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'GMRES.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Solving $\mathbf{A x = b}$ as an Optimization Problem
## 1.1 Idea
## 1.2 Derivation of the Algorithm
# 2. Basic Implementation and Example
