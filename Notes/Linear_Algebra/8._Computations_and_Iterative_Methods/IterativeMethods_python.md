
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'IterativeMethods_python.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Code: Monitor the Evolution of an Iterative Scheme
# 2. Iterative Solutions of $\mathbf{A x = b}$
## 2.1 Idea: Set up a Fixed Point
## 2.2 Convergence
## 2.3 Jacobi Iteration
## 2.4 Gauss Seidel Iteration (GS)
## 2.5 Successive Overrelaxation (SOR)
# 3. Iterative Methods for $\mathbf{A x = \lambda x}$
## 3.1 Power Method
## 3.2 Inverse Power Method
