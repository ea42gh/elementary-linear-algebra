
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '29_SVDapplications.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. The Eckart-Young Theorem
# 2. Low Rank Approximation of an Image
## 3. Principal Component Analysis
# 4. Some Web Resources
