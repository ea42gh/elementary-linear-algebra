---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'CR_Decomposition.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Example
## 1.1 Reduction of a Matrix to Row Echelon Form
## 1.2 The first rank(A) Columns of $\mathbf{E^{-1}}$ are the Pivot Columns of $\mathbf{A}$
## 1.3 An $\ \mathbf{ A = C R}\ $ Decomposition
# 2. Some Observations
## 2.1 A CR Decomposition Reveals Bases for the Column and Row Spaces of a Matrix
## 2.2 A CR Decomposition is Not Unique
## 2.3 A CMR Decomposition
