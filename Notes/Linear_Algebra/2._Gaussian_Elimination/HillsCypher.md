---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'HillsCypher.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Hill's Cipher
## 1.1 Overview
## 1.2 Transform a Message into a Matrix
## 1.3 Encode the Message
## 1.4 Decode the Message
# 2. Avoiding the Computation of a Matrix Inverse
