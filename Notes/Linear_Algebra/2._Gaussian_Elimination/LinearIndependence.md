---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'LinearIndependence.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Linear Independence
# 1.1 Model $A x = b$ Problem
# 2. Checking Linear Independence of a set of vectors in $\mathbb{F}^{\small{N}}$
## 2.1 Easy case 1: The set of vectors contains the zero vector
## 2.2 Easy case 2: One of the vectors is recognized as a linear combination of the other vectors.
## 2.3 Easy case 3: More vectors than entries in a vector
# 2.4 Easy case 4: The vectors contain a triangular matrix
# 3: Linear Independence of Functions
## 3.1 Polynomials
