---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '08_LinearIndependence.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Right Hand Sides $b$
# 2. Solutions of $A x\ =\ b$
## 2.1 The Number of Solutions
## 2.2 Particular and Homogeneous Solutions
## 2.3 Non-trivial Homogeneous Solutions
# 3. Linear Independence of Vectors
## 3.1 Definition and Theorem
## 3.2 Examples
### 3.2.1 General Case
### 3.2.2 Special Cases
# 4. Take Away
