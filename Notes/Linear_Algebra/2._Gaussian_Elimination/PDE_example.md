---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'PDE_example.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Numerical Approximation of Derivatives
## 1.1 First Derivative
## 1.2 Second Derivative
# 2. Example: Discretize the Poisson Equation
## 2.1 Discretize the Problem
## 2.2 Python Implementation
## 2.3 Solve the Resulting Problem
