---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'GivensRotations.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Givens Rotation Algorithm
## 1.1 Small Example with Rationals
## 1.2 Floating Point Example
## 1.3 Remark: this is a QR Decomposition
