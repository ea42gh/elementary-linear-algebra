---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'EigenPbApplications.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1 Eigendecomposition
# 2. Powers of $A$
## 2.1 Integer Power of $A$
## 2.2 Non-negative Power of $A$
# 3. Functions of $A$
# 4. Application: Difference Equations
# 5. Application: Linear System of ODEs
