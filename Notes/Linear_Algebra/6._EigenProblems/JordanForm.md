---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'JordanForm.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Jordan Form of a Matrix
## 1.1 Definitions and Theorem
## 1.2 Example
# 2. Naive Computation of a Jordan Form
## 2.1 Jordan Chains
## 2.2 Naive Computation Example
# 3. Computation of a Jordan Form
## 3.1 Point Diagram and its Properties
### 3.1.1 The Diagram
### 3.1.2 Nested Null Spaces
### 3.1.3 The Algorithm
## 3.2 Computation of a Jordan Form Example
### 3.2.1  Step 1: Compute the Eigenvalues of A and their Algebraic Multiplicities
### 3.2.2 Step 2: Compute the Row Echelon Forms $\mathbf{(A-\lambda I)^k}$
### 3.2.3 Step 3: Compute the Nested Set of Basis Vectors
### 3.2.4 Step 4: Assemble the matrices
