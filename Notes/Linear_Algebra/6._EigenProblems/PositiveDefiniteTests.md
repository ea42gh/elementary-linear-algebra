---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'PositiveDefiniteTests.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Sylvester's Law of Inertia
# 2. GE for Symmetric Matrices
## 2.1. Case: No Row Exchange Required
## 2.2 Case: Row Exchanges Required
### 2.2.1  Subcase: Require Row Exchange, Pivot Exists
## 2.3 Case: Row Exchange Required, Missing Pivots 
# 3. Take Away
