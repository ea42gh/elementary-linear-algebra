
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'ComplexEigenProblems.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Complex Numbers
## 1.1 Definition and Basic Operations
#### 1.1.1 **Examples:**
#### 1.1.2 **Operations** using complex numbers are defined as follows
#### 1.1.3 **First Geometric Interpretation**
## 1.2 Polar Representation, Euler's Formula
#### 1.2.1 **Polar Representation**
#### 1.2.2 **Euler's Formula and Complex Multiplication**
#### 1.2.3 Geometric Interpretation of Multiplication by $e^{i \phi}$
#### 1.2.4 **Composition of Rotation and Scaling Matrices**
## 1.3 Vectors With Complex Entries
## 1.4 Takeaway
# 2 Complex Eigendecompositions
## 2.1 A 2x2 Example to Motivate Some Theory
## 2.2 A Larger Example
