
---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '18_EigenComputations.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Stochastic Matrices
# 2. Null Space Computations
# 3. Matrices of Size $2 \times 2$
# 4. Matrices of Size $3 \times 3$ with a Known Non-zero Eigenvalue
# 5. Determinants that Can be Factored
