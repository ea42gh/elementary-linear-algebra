---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'MatrixMultApplication_GraphTheory.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Graphs and Adjacency Matrices
## 1.1 Definitions
## 1.2 Multiplication by a Row Vector from the Left
### 1.2.1 Pick a Row of A
### 1.2.2 Sum two Rows of A
### 1.2.3 Pick a Row and Multiply by $A$ Twice
## 1.3 Powers of A
# 2. Counting Triangles
# 3. Take Away
