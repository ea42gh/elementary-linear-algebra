---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: '04_MatrixAlgebra.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Algebra
## 1.1 Preliminary Remarks: Addition and Scalar Multiplication
## 1.2 The Product $\mathbf{ A B }$
## 1.3 Properties of Matrix Multiplication
## 1.4 The Dot Product Compared to Matrix Multiplication
# 2. Examples
## 2.1 Products of More than 2 Matrices
## 2.2 Substitution is Matrix Multiplication
# 3. Take Away
