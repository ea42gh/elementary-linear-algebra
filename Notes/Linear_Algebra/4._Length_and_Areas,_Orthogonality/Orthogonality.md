---
topic: 'Linear Algebra'
media:
    youtube: ''
    notebook: 'Orthogonality.ipynb'
status: ''
tags:   ['LinearAlgebra']
up: [[INDEX]]
prev: ''
---

# 1. Inner Product Spaces and Metrics
## 1.1 Basic Definitions
## 1.2 Inequalities, Angle, Orthogonal Vectors
## 1.3 Fundamental Theorem of Linear Algebra (Part 2)
### 1.3.1 Main Definitions and Theorem
#### 1.3.1.1 Linear Independence of Orthogonal Vectors
#### 1.3.1.2 Orthogonal Spaces
### 1.3.2 Use the Fundamental Theorem to Decompose a Vector (Naive Method)
### 1.3.3 Use the Normal Equation to Decompose a Vector
# 2. The Normal Equation
## 2.1 Basic Properties of the Normal Equation
## 2.2 Examples
### 2.2.1 Split a Vector
### 2.2.2 Distance of a vector from a hyperplane
### 2.2.3 Special Case: the columns of $A$ are mutually orthogonal
# 3. Projection Matrices, Orthogonal Matrices and Unitary Matrices
## 3.1 Orthogonal Projection Matrices
### 3.1.1 Theory
### 3.1.2 Examples
## 3.2 Orthogonal Matrices and Unitary Matrices
### 3.2.1 A Naive Construction Method for Orthogonal Matrices
### 3.2.2 Important Examples
### 3.2.3 Important Properties of Orthogonal and Unitary Matrice
## 3.3 Gramm-Schmidt Orthogonalization
# 4. Gramm-Schmidt Orthogonalization ...
