{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "using LinearAlgebra"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:60px;color:darkred;font-size:40px;\">The Modified Gram-Schmidt Algorithm</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Modified Gram-Schmidt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Gram-Schmidt process performs poorly numerically: the resulting vectors are often not quite orthogonal due to rounding errors.\n",
    "\n",
    "$\\qquad$ An improved version that results in smaller errors in finite precision arithmetic removes the parallel components\n",
    "as soon as they are available<br>\n",
    "$\\qquad$ we know $A = Q R:$ we cans solve for each $q_i$ and a row of $R$ at a time.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 The Vector $\\mathbf{q_1}$ and the first Row of $\\mathbf{R}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We know $A = Q R$. Partitioning the $Q$ matrix into  $\\left( \\begin{array}{c|ccc} q_1 & q_2 & \\dots q_n \\end{array} \\right)$\n",
    "and $R = \\left( \\begin{array}{c|ccc} r_{1 1} & r_{1 2} & \\dots & r_{1 n} \\\\ \\hline\n",
    "                                     0       & r_{2 2} & \\dots & r_{2 n} \\\\\n",
    "                                     0       & 0       & \\dots & r_{3 n} \\\\\n",
    "                                     \\dots   & \\dots   & \\dots & \\dots \\\\\n",
    "                                     0       & 0       & \\dots & r_{n n}\n",
    "                                     \\end{array}\\right)$, we obtain\n",
    "\n",
    "$\\qquad\\begin{align}\n",
    "a_1 &= r_{1 1}\\ q_1                & \\Leftrightarrow \\quad & r_{1 1}\\ q_1 = a_1 \\\\\n",
    "a_2 &= r_{1 2}\\ q_1 + r_{2 2}\\ q_2 & \\Leftrightarrow \\quad & r_{1 2}\\ q_1 = a_2 - r_{2 2}\\ q_2 \\\\\n",
    "\\dots& & &\\\\\n",
    "a_n &= r_{1 n}\\ q_1 + r_{2 n}\\ q_2 + \\dots r_{n n}\\ q_n \\quad& \\Leftrightarrow \\quad & r_{1 n}\\ q_1  = a_n - + r_{2 n}\\ q_2 \\dots - r_{n n}\\ q_n\n",
    "\\end{align}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Which we can solve for the $q_1$ and the first row of the $R$ matrix by taking dot products with the $q_1$ vector:\n",
    "\n",
    "$\\qquad\\begin{align}\n",
    "r_{1 1} &= \\Vert a_1 \\Vert,\\quad q_1 = \\frac{1}{r_{1 1}} a_1 \\\\\n",
    "r_{1 i} &= a_i \\cdot q_1,\\quad i=2,3,\\dots n\n",
    "\\end{align}$\n",
    "\n",
    "since the $q_i$ are orthonormal."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Each of the Remaining Vectors $\\mathbf{q_i}$ and Corresponding Rows of $\\mathbf{R}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The remaining equations are $A_2 = Q_2 R_2,$<br>\n",
    "$\\qquad$ where $A_2 = \\left( a_2\\;\\dots\\;a_n \\right)- q_1 \\left( r_{1 2} \\; \\dots r_{1 n} \\right),\\quad Q_2 = \\left( q_2 \\dots q_n \\right) \\;\\;$ and\n",
    "$\\;\\;R_2 = \\left( \\begin{array}{ccc} r_{2 2} & \\dots & r_{2 n} \\\\\n",
    "                                     0       & \\dots & r_{3 n} \\\\\n",
    "                                     \\dots   & \\dots & \\dots \\\\\n",
    "                                     0       & \\dots & r_{n n}\n",
    "                                     \\end{array}\\right)$\n",
    "\n",
    "Since $R_2$ is upper triangular, this new problem is solved as before,<br>\n",
    "$\\qquad$ with each of the columns in $A_2$ given by\n",
    "$a^{(1)}_i = a_i - r_{1 i} q_1, \\quad i=2,3,\\dots n$.\n",
    "\n",
    "$\\qquad\\begin{align}\n",
    "r_{2 2} &= \\Vert a^{(1)}_2 \\Vert, \\quad q_2 = \\frac{1}{r_{2 2}} a^{(1)}_2 \\\\\n",
    "r_{2 i} &= a^{(1)}_i \\cdot q_2, \\quad i=3, \\dots n\n",
    "\\end{align}$\n",
    "\n",
    "The result is a reduced problem that again has the same form.\n",
    "\n",
    "The modified Gram-Schmidt procedure computes the $R$ matrix one row at a time.<br>\n",
    "Below a reference implementation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "function modified_gram_schmidt(A)\n",
    "    # orthogonalises the columns of the input matrix A, updating the vectors in a copy of A: A -> Q\n",
    "    num_vectors = size(A)[2]\n",
    "    Q           = copy(A)\n",
    "\n",
    "    for i = 1:num_vectors\n",
    "        r_ii = norm(Q[:, i])\n",
    "        Q[:, i] = Q[:, i] / r_ii               # compute the new q\n",
    "        for j = (i+1) : num_vectors            # for each remaining vector\n",
    "            r_ij     = dot(Q[:, j], Q[:, i])\n",
    "            Q[:, j] -= r_ij * Q[:, i]          #     update remaing vectors in A\n",
    "        end\n",
    "    end\n",
    "    return Q\n",
    "end;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Gram-Schmidt procedure by contrast computes the entries of $R$ one column at a time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "function classical_gram_schmidt(A)\n",
    "    # orthogonalises the columns of the input matrix A, updating the vectors in a copy of A: A -> Q\n",
    "    vec_size,num_vectors = size(A)\n",
    "    Q                    = copy(A)\n",
    "\n",
    "    for i = 1:num_vectors\n",
    "        sum = zeros(vec_size)               # compute the orthogonal projection of a_i onto the current span\n",
    "        for j = 1:(i-1)\n",
    "            r_ji = dot(Q[:, j], Q[:, i])\n",
    "            sum +=r_ji * Q[:, j]\n",
    "        end\n",
    "        Q[:, i] -= sum                      # compute the new w_i (the orthognal part of a_i)\n",
    "\n",
    "        r_ii = norm(Q[:,i])\n",
    "        Q[:, i] = Q[:, i] / r_ii            # normalize w_i to q_i\n",
    "    end\n",
    "    return Q\n",
    "end;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MQ' * MQ ≈ I = true\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "4×4 Matrix{Float64}:\n",
       " 0.816497   0.471405  -0.09245   0.320256\n",
       " 0.408248  -0.235702  -0.3698   -0.800641\n",
       " 0.0        0.471405   0.7396   -0.480384\n",
       " 0.408248  -0.707107   0.5547    0.160128"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A = [2. 6 5 9;\n",
    "     1 2 1 -7;\n",
    "     0 1 2 4;\n",
    "     1 1 1 8\n",
    "]\n",
    "MQ=modified_gram_schmidt(A)\n",
    "@show MQ'MQ ≈ I\n",
    "MQ"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MQ ≈ CQ = true\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "4×4 Matrix{Float64}:\n",
       " 0.816497   0.471405  -0.09245   0.320256\n",
       " 0.408248  -0.235702  -0.3698   -0.800641\n",
       " 0.0        0.471405   0.7396   -0.480384\n",
       " 0.408248  -0.707107   0.5547    0.160128"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "CQ = classical_gram_schmidt(A)\n",
    "@show MQ ≈ CQ\n",
    "CQ"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Explore"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try various matrices with different sizes and condition numbers.\n",
    "* How orthogonal are the resulting matrices?\n",
    "* Investigate the errors using the Householder Algorithm instead"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.11.1",
   "language": "julia",
   "name": "julia-1.11"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.11.1"
  },
  "toc-autonumbering": true
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
