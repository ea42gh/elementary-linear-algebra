{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e80bafa7-c784-4208-ada3-52e9e8fe7a64",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "using Pkg, Revise\n",
    "gla_dir = \"../GenLinAlgProblems\"\n",
    "Pkg.activate(gla_dir)\n",
    "\n",
    "using GenLinAlgProblems, LinearAlgebra, Latexify, Printf, SymPy\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4367e54-36fa-4c6b-b255-5a1a13f007cc",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align:center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">Condition Number</strong>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f041336-efbc-48f1-8bf7-28bf1ddb1869",
   "metadata": {},
   "source": [
    "The examples in this notebook were taken from the following [reference: julia.quantecon.org](https://julia.quantecon.org/tools_and_techniques/iterative_methods_sparsity.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "554a8508-aedb-4d14-9040-91a504d1ff5d",
   "metadata": {},
   "source": [
    "# 1. Some Comments and Definition"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12da98dc-10af-4753-8bc3-132a745a61a5",
   "metadata": {},
   "source": [
    "Formally, the condition number of a function measures how much the output value of the function can change for a small change in the input argument.<br>\n",
    "Rather than providing a detailed analysis, the following observation will serve as sufficient motivation.\n",
    "\n",
    "Finite precision arithmetic with numbers of different sizes presents a problem when numbers are of different sizes.<br>\n",
    "$\\qquad$ E.g., consider the following addition with 4 significant figures\n",
    "\n",
    "\\begin{align}\n",
    "&111.1       &   \\\\\n",
    "&\\,\\,\\;\\;0.01231 &   \\\\\n",
    "&\\rule{2cm}{0.4pt} & \\\\\n",
    "&111,1\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "640ab58a-b6a6-4d0f-a945-661e71700990",
   "metadata": {},
   "source": [
    "The SVD of a matrix $A = U \\Sigma V^t$ allows us to estimate the relative sizes of numbers in a computation involving $A$\n",
    "such as $A x$.\n",
    "\n",
    "The orthogonal matrices $U$ and $V$ applied to a vector do not change the length of the vector, e.g., $\\Vert U x \\Vert = \\Vert x \\Vert$.<br>\n",
    "Significant changes are due to the singular values in $\\Sigma$. The condition number measures the relative sizes of the largest to the smallest singular value:\n",
    "\n",
    "$\\qquad\n",
    "C(A) = \\frac{\\sigma_{max}(A)}{\\sigma_{min}(A)}\n",
    "$\n",
    "\n",
    "**Remarks:**\n",
    "* For symmetric matrices, $\\sigma_{max} = \\max_{i}\\vert \\lambda_i \\vert$, and $\\sigma_{min} = \\min_{i}\\vert \\lambda_i \\vert$, where $\\lambda_i, i=1,...$ are the eigenvalues of $A$.<br>\n",
    "$\\qquad\n",
    "C(A) = \\frac{max_i \\vert \\lambda_i(A) \\vert}{min_i \\vert \\lambda_i(A) \\vert }\n",
    "\\quad$\n",
    "* The definitions depend on the chosen matrix norm: there exist other definitions of condition numbers.\n",
    "<br>\n",
    "* **The larger the condition number,** the less well behaved computations with $A$.\n",
    "* Computing the condition number is expensive, use it judiciously!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47b46df9-de64-4a9a-891c-e9f50541056c",
   "metadata": {},
   "source": [
    "# 2 Examples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "25b875f0-907a-4482-8138-d1d3e3aba6be",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.0e-6"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "@syms e::(positive)\n",
    "@syms lambda::(real)\n",
    "\n",
    "ϵ = 1e-6"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3713435f-d56f-43ad-b654-d3668eeec6db",
   "metadata": {},
   "source": [
    "#### **2 x 2 Examples**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "0951fc1b-55b6-454b-91e4-3139dcfb03c7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The condition number for an identity matrix\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "1.0"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"The condition number for an identity matrix\")\n",
    "cond(1I(2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "f9a85002-10eb-4c9f-bef1-dcaed0331071",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The condition number of a matrix that is almost singular:\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\n",
       "\\left[\n",
       "\\begin{array}{cc}\n",
       "1 & 0 \\\\\n",
       "1 & e \\\\\n",
       "\\end{array}\n",
       "\\right]\n",
       "\\end{equation}\n"
      ],
      "text/plain": [
       "L\"\\begin{equation}\n",
       "\\left[\n",
       "\\begin{array}{cc}\n",
       "1 & 0 \\\\\n",
       "1 & e \\\\\n",
       "\\end{array}\n",
       "\\right]\n",
       "\\end{equation}\n",
       "\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Condition number for   e = 1.0e-6\n",
      ".  C(A)   = 2.0e+06\n"
     ]
    }
   ],
   "source": [
    "println( \"The condition number of a matrix that is almost singular:\")\n",
    "A = [1 0\n",
    "     1 e ]\n",
    "display(latexify(A))\n",
    "\n",
    "println(\"\\nCondition number for   e = $ϵ\")\n",
    "@printf \".  C(A)   = %.1e\\n\" cond( N.(subs.(  A, e, ϵ )))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25970b9f-341d-4558-adbd-7683d8a4d9c1",
   "metadata": {},
   "source": [
    "#### **3 x 4 Example, Product of Matrices**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d37827bc-c712-4876-beae-b2aa9e22d793",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The condition number of a product of matrices can change radically\n",
      "\n",
      "consider the following matrix L =\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\n",
       "\\left[\n",
       "\\begin{array}{cccc}\n",
       "1 & e & 0 & 0 \\\\\n",
       "1 & 0 & e & 0 \\\\\n",
       "1 & 0 & 0 & e \\\\\n",
       "\\end{array}\n",
       "\\right]\n",
       "\\end{equation}\n"
      ],
      "text/plain": [
       "L\"\\begin{equation}\n",
       "\\left[\n",
       "\\begin{array}{cccc}\n",
       "1 & e & 0 & 0 \\\\\n",
       "1 & 0 & e & 0 \\\\\n",
       "1 & 0 & 0 & e \\\\\n",
       "\\end{array}\n",
       "\\right]\n",
       "\\end{equation}\n",
       "\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "and Lᵗ L =\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "\\begin{equation}\n",
       "\\left[\n",
       "\\begin{array}{cccc}\n",
       "3 & e & e & e \\\\\n",
       "e & e^{2} & 0 & 0 \\\\\n",
       "e & 0 & e^{2} & 0 \\\\\n",
       "e & 0 & 0 & e^{2} \\\\\n",
       "\\end{array}\n",
       "\\right]\n",
       "\\end{equation}\n"
      ],
      "text/plain": [
       "L\"\\begin{equation}\n",
       "\\left[\n",
       "\\begin{array}{cccc}\n",
       "3 & e & e & e \\\\\n",
       "e & e^{2} & 0 & 0 \\\\\n",
       "e & 0 & e^{2} & 0 \\\\\n",
       "e & 0 & 0 & e^{2} \\\\\n",
       "\\end{array}\n",
       "\\right]\n",
       "\\end{equation}\n",
       "\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Condition number for   e = 1.0e-6\n",
      ".  C(L)   = 1.73e+06\n",
      ".  C(L'L) = 3.43e+28\n"
     ]
    }
   ],
   "source": [
    "println(\"The condition number of a product of matrices can change radically\")\n",
    "läuchli(N, ϵ) = [ones(Int,N)'; ϵ * I(N)]'\n",
    "\n",
    "L   = läuchli(3, e)\n",
    "LtL = L'L\n",
    "\n",
    "println( \"\\nconsider the following matrix L =\")\n",
    "display(latexify(L))\n",
    "println( \"\\nand Lᵗ L =\")\n",
    "display(latexify(LtL))\n",
    "\n",
    "println(\"\\nCondition number for   e = $ϵ\")\n",
    "@printf \".  C(L)   = %.2e\\n\" cond( N.(subs.(   L, e, ϵ )))\n",
    "@printf \".  C(L'L) = %.2e\\n\" cond( N.(subs.( LtL, e, ϵ )))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7cc299c6-356a-485d-9c3d-0aa1e572c6b6",
   "metadata": {},
   "source": [
    "____\n",
    "Theoretical computation: $L^t L$ has an eigenvalue 0 with multiplicity 1 and and eigenvalue $e^2$ with algebraic multiplicity 2.<br>\n",
    "$\\qquad$ The remaining eigenvalue is $trace(A) - 2 e^2 = 3$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "5e9d324e-199a-4c8f-be78-6d35f1fe4afd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "distinct eigenvalues: Sym{PyCall.PyObject}[0 e^2 e^2 + 3]\n"
     ]
    }
   ],
   "source": [
    "evals_LtL = SymPy.solve(det(LtL-lambda*I), lambda)'\n",
    "println(\"distinct eigenvalues: \", evals_LtL )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "d1c1c454-da6b-467d-a0e0-3b00c14b8656",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ".  C(L)   = 1.73e+06\n"
     ]
    }
   ],
   "source": [
    "@printf \".  C(L)   = %.2e\\n\" subs( sqrt(evals_LtL[3]/evals_LtL[2]), e, ϵ)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "9eb5dbcc-e059-447f-9766-aa1eea3d90a2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ".  C(LᵗL)   = 3.00e+12\n"
     ]
    }
   ],
   "source": [
    "@printf \".  C(LᵗL)   = %.2e\\n\" subs( evals_LtL[3]/evals_LtL[2], e, ϵ)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5ed6d3a-5756-448b-a10c-5f0b7ad1b870",
   "metadata": {},
   "source": [
    "____\n",
    "Why the discrepancy? cond() uses the svd to compute the condition number:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "3d9cf734-4d2c-4f3c-96c3-370ea3795b5e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Vector{Float64}:\n",
       " 1.7320508075691659\n",
       " 1.0e-6\n",
       " 1.0e-6"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# The SVD of L  returns\n",
    "svd( N.(subs.(L,e,ϵ))).S"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "5c640079-9d67-4f04-8460-5b84873e3393",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4-element Vector{Float64}:\n",
       " 3.0000000000009996\n",
       " 1.0e-12\n",
       " 1.0e-12\n",
       " 8.744621874862984e-29"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# The SVD of Lᵗ L  returns\n",
    "svd( N.(subs.(LtL,e,ϵ))).S"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "88de71c1-ed0b-42ac-9960-28d721e06f79",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Theoretical values\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[\\begin{smallmatrix}0 & 1.0 \\cdot 10^{-12} & 3.000000000001\\end{smallmatrix}\\right]$"
      ],
      "text/plain": [
       "1×3 Matrix{Sym{PyCall.PyObject}}:\n",
       " 0  1.00000000000000e-12  3.00000000000100"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"Theoretical values\")\n",
    "subs.(evals_LtL, e, 1e-6)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23c5b376-fd22-478d-954d-8c7b47d63fc8",
   "metadata": {},
   "source": [
    "# 3. Take Away"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40cff304-d190-406e-9d62-4e106f8974bb",
   "metadata": {},
   "source": [
    "* The value of the condition number is an indicator of numerical difficulties for computations involving a matrix $A$.\n",
    "* It is better to choose algorithms based on $A$ rather than $A^t A$, e.g., for linear regression computations<br>\n",
    "$\\qquad$ i.e., choose a more appropriate algorithm\n",
    "<br><br>Damping: Solve $argmin_x \\left( \\Vert A x -b \\Vert^2 + \\Vert \\lambda x \\Vert^2 \\right)$ rather than $argmin_x \\left( \\Vert A x -b \\Vert^2\\right)$<br><br>\n",
    "* choose alternative representations of the data resulting in matrices with lower condition number\n",
    "* transform the problem using a suitable **preconditioner** $P$, e.g., solve $A x = b$ in two steps: $A P P^{-1} x = b \\Leftrightarrow (A P) \\tilde{x} = b, x = P \\tilde{x}$,<br>\n",
    "$\\qquad$ where $A P$ has much lower condition number compared to $A$."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.11.1",
   "language": "julia",
   "name": "julia-1.11"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
