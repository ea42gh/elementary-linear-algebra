{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "d52b1b62-2d78-4fa8-a546-805e1f11100b",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "using Pkg, Revise\n",
    "Pkg.activate(\"../GenLinAlgProblems\")\n",
    "using GenLinAlgProblems, LinearAlgebra, Random, LaTeXStrings, Latexify\n",
    "\n",
    "Random.seed!(113312);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4552a5c-3a09-4cc9-831c-3e45ec37d05c",
   "metadata": {},
   "source": [
    "<div style=\"float:center;width:100%;text-align: center;\"><strong style=\"height:60px;color:darkred;font-size:40px;\">Updating a Matrix Inverse</strong></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c34f1d2-6f25-42ba-a2f8-86c597f06b3e",
   "metadata": {},
   "source": [
    "# 1. The Sherman-Morrison-Woodbury Formula"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b76b1878-6361-434f-8b01-fa23bf67b1ac",
   "metadata": {},
   "source": [
    "## 1.1. Derivation"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "11c9adb5-d428-44b0-9ae7-7ff31e56e6aa",
   "metadata": {},
   "source": [
    "Consider the block matrix $\\begin{pmatrix} A & U \\\\ V & D \\end{pmatrix}$, where $A$ and $D$ are square.\n",
    "\n",
    "Assuming that $A^{-1}$ exists, we can eliminate $V$ and $U$<br>\n",
    "$\\qquad\\begin{align}\n",
    "\\begin{pmatrix} A & U \\\\ V & D \\end{pmatrix}\n",
    "=& \\begin{pmatrix} I & 0 \\\\ V A^{-1} & I \\end{pmatrix} \\begin{pmatrix} A & U \\\\ 0 & S_A \\end{pmatrix} \\\\\n",
    "=& \\begin{pmatrix} I & 0 \\\\ V A^{-1} & I \\end{pmatrix} \\begin{pmatrix} A & 0 \\\\ 0 & S_A \\end{pmatrix} \\begin{pmatrix} I & A^{-1}U \\\\ 0 & I \\end{pmatrix}\n",
    "\\end{align}$\n",
    "\n",
    "where we have defined the **Schur Component** $S_A = D - V A^{-1} U$.\n",
    "\n",
    "Further assuming that $A_A^{-1}$ exists, we can invert this equation to obtain<br><br>\n",
    "$\\qquad \\begin{pmatrix} A & U \\\\ V & D \\end{pmatrix}^{-1} =\n",
    " \\begin{pmatrix} I & -A^{-1}U \\\\ 0 & I \\end{pmatrix}\n",
    " \\begin{pmatrix} A^{-1} & 0 \\\\ 0 & S_A^{-1} \\end{pmatrix} \\begin{pmatrix} I & 0 \\\\ -V A^{-1} & I \\end{pmatrix} \\qquad\\qquad\\qquad\\qquad (1)\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5c81b5c-75dc-4b3c-9831-68647f3fc918",
   "metadata": {},
   "source": [
    "We can similarly obtain a formula for this inverse assuming that $D^{-1}$ exists<br>\n",
    "\n",
    "$\\qquad \\begin{pmatrix} A & U \\\\ V & D \\end{pmatrix}^{-1} =\n",
    " \\begin{pmatrix} I & 0 \\\\ -D^{-1}V & I \\end{pmatrix} \n",
    " \\begin{pmatrix} S_D^{-1} & 0 \\\\ 0 & D^{-1} \\end{pmatrix} \\begin{pmatrix} I & - U D^{-1} \\\\ 0 & I \\end{pmatrix} \\qquad\\qquad\\qquad\\qquad (2)\n",
    "$\n",
    "\n",
    "assuming the **Schur Component** $S_D = A - U D^{-1} V$ is also invertible."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18cf50a7-d3c7-46f7-a4df-027024d33979",
   "metadata": {},
   "source": [
    "Multiplying out the matrices in Eq 1 and Eq 2 and comparing terms, we obtain the Sherman-Morrison-Woodbury Formula<br>\n",
    "\n",
    "$\\qquad S_D^{-1} = A^{-1} + A^{-1} U S_A^{-1} V A^{-1} \\Leftrightarrow \\color{blue}{\\left( A - U D^{-1} V \\right)^{-1} = A^{-1} + A^{-1} U\\; \\left( D - V A^{-1} U \\right)^{-1} \\; V A^{-1} \\qquad\\qquad\\qquad (3)}$\n",
    "\n",
    "and<br>\n",
    "$\\qquad D S_A^{-1} V = V S_D^{-1} A$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "659aee88-7f0c-4bed-a0c2-1dbf95c775f5",
   "metadata": {},
   "source": [
    "#### Example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8849fac-e76d-45de-a6c4-3e8bb99b20c7",
   "metadata": {},
   "source": [
    "Consider the matrix $M = \\left(\\begin{array}{rr|r}\n",
    "1 & -2 & -3 \\\\\n",
    "-1 & 3 & 2 \\\\ \\hline\n",
    "-3 & 5 & 11\n",
    "\\end{array}\\right),\\;\\;$\n",
    "with\n",
    "$A = \\left(\\begin{array}{rr} 1 & -2 \\\\ -1 & 3 \\end{array}\\right), \\;\\; U = \\left(\\begin{array}{r} -3 \\\\ 2 \\end{array} \\right),\\;\\; V = \\left( -3\\;\\;5 \\right)\\;\\;$ and $\\;\\;D= \\left(11 \\right)$\n",
    "\n",
    "The Schur Components are $S_A = ( 1 )\\;\\;$ and $\\;\\; S_D = \\frac{1}{11} \\left( \\begin{array}{rr} 2 & -7 \\\\ -5 & 23 \\end{array}\\right)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "9abef9d1-22c1-402e-8773-3ee156fcacb6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "inv(A - U * D_inv * V) == A_inv + A_inv * U * inv(S_A) * V * A_inv = true\n"
     ]
    }
   ],
   "source": [
    "# Check\n",
    "M = [1 -2 -3; -1 3 2; -3 5 11]\n",
    "A = M[1:2,1:2]; U = M[1:2,3:3]; V = M[3:3, 1:2]; D = M[3:3,3:3]\n",
    "A_inv = inv(Rational{Int}.(A))\n",
    "D_inv = inv(Rational{Int}.(D))\n",
    "S_A = D - V * A_inv * U\n",
    "S_D = A - U * D_inv * V\n",
    "\n",
    "@show inv(A-U*D_inv*V) == (A_inv + A_inv*U *inv(S_A) * V*A_inv);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7cb3072b-8eda-4615-8802-0f36dec0c3fd",
   "metadata": {},
   "source": [
    "## 1.2. Application: Rank 1 Update of a Matrix Inverse"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bbe73df-b135-40d4-9438-0a1f017b19e1",
   "metadata": {},
   "source": [
    "Let $D = -I, U = u, V = v^t$ in the Sherman-Morrison-Woodbury Formula. We obtain<br>\n",
    "\n",
    "$\\qquad \\begin{align} \\left(A + u v^t \\right)^{-1} &= A^{-1} - A^{-1} u\\ (I+v^t A^{-1} u)^{-1}\\ v^t A^{-1} \\\\\n",
    "&= A^{-1} - \\frac{1}{1+v^t A^{-1} u} A^{-1} u v^t A^{-1} \\end{align}$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8acd9f7e-a7e7-43e5-9a71-bc5167f43b32",
   "metadata": {},
   "source": [
    "Given $A^{-1}$, the inverse of the rank 1 update $(A + u v^t)$ is obtained by a number of matrix multiplications<br>\n",
    "$\\qquad$ since $(I + v^t A^{-1} u)$ is a size $1 \\times 1$ matrix that is trivially invertible."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ead05bbb-3423-4433-a8c5-3fbdd2cc283b",
   "metadata": {},
   "source": [
    "#### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "f306ff15-116a-4085-adc5-39ec6151c197",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mOriginal Matrix A and its inverse\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad\\qquad A =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "1 & 2 & 1 & 2 & 2 \\\\\n",
       "-2 & -3 & -3 & -4 & -3 \\\\\n",
       "-2 & -2 & -3 & -6 & -2 \\\\\n",
       "-2 & -6 & -1 & -1 & -7 \\\\\n",
       "2 & 6 & -2 & 8 & 7 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad A^{-1} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "51 & 52 & -27 & -8 & -8 \\\\\n",
       "-2 & -7 & 5 & 2 & 1 \\\\\n",
       "-14 & -14 & 7 & 2 & 2 \\\\\n",
       "-6 & -6 & 3 & 1 & 1 \\\\\n",
       "-10 & -6 & 2 & 0 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad\\qquad A =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "1 & 2 & 1 & 2 & 2 \\\\\n",
       "-2 & -3 & -3 & -4 & -3 \\\\\n",
       "-2 & -2 & -3 & -6 & -2 \\\\\n",
       "-2 & -6 & -1 & -1 & -7 \\\\\n",
       "2 & 6 & -2 & 8 & 7 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad A^{-1} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "51 & 52 & -27 & -8 & -8 \\\\\n",
       "-2 & -7 & 5 & 2 & 1 \\\\\n",
       "-14 & -14 & 7 & 2 & 2 \\\\\n",
       "-6 & -6 & 3 & 1 & 1 \\\\\n",
       "-10 & -6 & 2 & 0 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mRank 1 Update\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad\\qquad$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "0.044 & 0.022 & 0.019 & 0.03 & 0.048 \\\\\n",
       "0.081 & 0.041 & 0.035 & 0.054 & 0.087 \\\\\n",
       "0.015 & 0.008 & 0.007 & 0.01 & 0.016 \\\\\n",
       "0.016 & 0.008 & 0.007 & 0.011 & 0.017 \\\\\n",
       "0.014 & 0.007 & 0.006 & 0.01 & 0.015 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad\\qquad$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "0.044 & 0.022 & 0.019 & 0.03 & 0.048 \\\\\n",
       "0.081 & 0.041 & 0.035 & 0.054 & 0.087 \\\\\n",
       "0.015 & 0.008 & 0.007 & 0.01 & 0.016 \\\\\n",
       "0.016 & 0.008 & 0.007 & 0.011 & 0.017 \\\\\n",
       "0.014 & 0.007 & 0.006 & 0.01 & 0.015 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mUpdated Inverse\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad\\qquad$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "13.031 & 10.359 & -4.447 & -0.876 & -1.632 \\\\\n",
       "1.479 & -3.185 & 2.934 & 1.347 & 0.417 \\\\\n",
       "-3.642 & -2.641 & 0.848 & 0.057 & 0.263 \\\\\n",
       "-1.589 & -1.163 & 0.38 & 0.172 & 0.26 \\\\\n",
       "-4.228 & 0.33 & -1.429 & -1.083 & 0.032 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad\\qquad$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrr}\n",
       "13.031 & 10.359 & -4.447 & -0.876 & -1.632 \\\\\n",
       "1.479 & -3.185 & 2.934 & 1.347 & 0.417 \\\\\n",
       "-3.642 & -2.641 & 0.848 & 0.057 & 0.263 \\\\\n",
       "-1.589 & -1.163 & 0.38 & 0.172 & 0.26 \\\\\n",
       "-4.228 & 0.33 & -1.429 & -1.083 & 0.032 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "inv(A + uvt) ≈ A_inv - (1 / alpha) * (A_inv * u) * (v' * A_inv) = true\n"
     ]
    }
   ],
   "source": [
    "M = 5; k=1\n",
    "A, A_inv = gen_inv_pb(M, maxint=2)\n",
    "printstyled(\"Original Matrix A and its inverse\", color=:blue, bold=true)\n",
    "display(l_show( L\"\\qquad\\qquad A =\", A, L\"\\quad A^{-1} =\", A_inv))\n",
    "\n",
    "u        = 0.1*rand(M,1)\n",
    "v        = rand(M,1)\n",
    "uvt      = u*v'\n",
    "printstyled(\"Rank 1 Update\", color=:blue, bold=true)\n",
    "display(l_show(L\"\\qquad\\qquad\", round.(uvt, digits=3)))\n",
    "\n",
    "alpha    = (I+v'A_inv*u)[1,1]\n",
    "#inv( Real.( A+u*v' ))\n",
    "\n",
    "printstyled(\"Updated Inverse\", color=:blue, bold=true)\n",
    "display(l_show(L\"\\qquad\\qquad\", round.(A_inv - 1/alpha * (A_inv *u)*(v'*A_inv),digits=3)))\n",
    "\n",
    "@show inv(A+uvt) ≈ (A_inv - 1/alpha * (A_inv *u)*(v'*A_inv));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "daaaaf23-700a-48dc-aa88-f7551d011641",
   "metadata": {},
   "source": [
    "## 1.3. Application: Rank k Update of a Matrix Inverse"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ae6efd7-b541-410e-8c93-348178038f53",
   "metadata": {},
   "source": [
    "Consider updating an invertible matrix $A$ with a rank $k$ matrix $U V^t$, i.e.,<br>\n",
    "$\\qquad A + U V^t = A+u_1 v_1^t + \\dots u_k v_k^t$ \n",
    "\n",
    "Let $D = -I$ in the Sherman-Morrison-Woodbury Formula, and name the matrix $V^t$ rather than $V$. We obtain<br>\n",
    "\n",
    "$\\qquad\\qquad (A + U V^t)^{-1} = A^{-1} - A^{-1} U \\left( I + V^t A^{-1} U \\right)^{-1} V^t A^{-1},\n",
    "$ provided the inverses exist.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "add08632-8d22-4f64-9063-f9ff8e46bfd0",
   "metadata": {},
   "source": [
    "Note that for $A \\in \\mathscr{R}^{N \\times N},\\;\\;$ and $U, V \\in \\mathscr{R}^{N \\times k}.$<br>\n",
    "$\\qquad\\therefore\\;\\;$the matrix $I + V^t A^{-1} U$ has size $k \\times k$.\n",
    "\n",
    "The problem of inverting a size $N \\times N$ matrix $A + U V^t$ is reduced to inverting a matrix of size $k \\times k$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "2ab870ba-333d-4b50-ab10-2e4c5bf89d5c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mA and A⁻¹\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad A=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrrrrr}\n",
       "1 & 2 & 3 & 0 & 1 & -2 & -3 & -2 \\\\\n",
       "-1 & -1 & -5 & -1 & -2 & 5 & 5 & 2 \\\\\n",
       "-3 & -7 & -6 & 0 & -5 & 1 & 10 & 8 \\\\\n",
       "-1 & -3 & -3 & 4 & 8 & 5 & -4 & 0 \\\\\n",
       "-2 & -4 & -8 & 3 & 7 & 8 & 3 & 5 \\\\\n",
       "-3 & -4 & -11 & -6 & -18 & 11 & 13 & -2 \\\\\n",
       "-1 & -4 & 2 & 0 & -7 & -4 & -8 & -9 \\\\\n",
       "1 & 3 & 3 & 0 & 3 & -4 & 13 & 17 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad A^{-1} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrrrrr}\n",
       "-1628 & -547 & -352 & -43 & -33 & 46 & -3 & 52 \\\\\n",
       "375 & 111 & 77 & 3 & 19 & -7 & 3 & -10 \\\\\n",
       "248 & 92 & 56 & 11 & -2 & -9 & -1 & -9 \\\\\n",
       "-163 & -94 & -45 & -21 & 25 & 14 & 5 & 10 \\\\\n",
       "126 & 48 & 29 & 6 & -2 & -5 & -1 & -5 \\\\\n",
       "5 & 10 & 3 & 4 & -6 & -2 & -1 & -1 \\\\\n",
       "35 & -1 & 5 & -4 & 9 & 2 & 1 & 0 \\\\\n",
       "-62 & -9 & -11 & 3 & -9 & -1 & -1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "\"\\$\\\\qquad A=\\$\\$\\\\begin{equation}\\n\\\\left(\\n\\\\begin{array}{rrrrrrrr}\\n1 & 2 & 3 & 0 & 1 & -2 & -3 & -2 \\\\\\\\\\n-1 & -1 & -5 & -1 & -2 & 5 & 5 & 2 \\\\\\\\\\n-3 & -7 & -6 & 0 & -5 & 1 & 10 & 8 \\\\\\\\\\n-1 & -3 & -3 & 4 & 8 & 5 & -4 & 0 \\\\\\\\\\n-2 & -4 & -8 & 3 & 7 & 8 & 3 & 5 \\\\\\\\\\n-3 & -4 & -11 & -6 & -18 &\"\u001b[93m\u001b[1m ⋯ 295 bytes ⋯ \u001b[22m\u001b[39m\"& 56 & 11 & -2 & -9 & -1 & -9 \\\\\\\\\\n-163 & -94 & -45 & -21 & 25 & 14 & 5 & 10 \\\\\\\\\\n126 & 48 & 29 & 6 & -2 & -5 & -1 & -5 \\\\\\\\\\n5 & 10 & 3 & 4 & -6 & -2 & -1 & -1 \\\\\\\\\\n35 & -1 & 5 & -4 & 9 & 2 & 1 & 0 \\\\\\\\\\n-62 & -9 & -11 & 3 & -9 & -1 & -1 & 1 \\\\\\\\\\n\\\\end{array}\\n\\\\right)\\n\\\\end{equation}\\n\\$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "\u001b[34m\u001b[1mrank 3 Update\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad\\qquad$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrrrrr}\n",
       "-6 & -8 & 6 & -4 & 10 & 10 & 8 & -8 \\\\\n",
       "4 & 5 & -4 & 4 & -7 & -6 & -5 & 5 \\\\\n",
       "12 & 17 & -10 & 8 & -23 & -20 & -19 & 19 \\\\\n",
       "4 & 6 & -4 & 0 & -6 & -8 & -6 & 6 \\\\\n",
       "6 & 6 & -8 & 8 & -8 & -8 & -4 & 4 \\\\\n",
       "-12 & -19 & 8 & -4 & 25 & 22 & 23 & -23 \\\\\n",
       "-6 & -8 & 6 & -4 & 10 & 10 & 8 & -8 \\\\\n",
       "-24 & -28 & 28 & -24 & 36 & 36 & 24 & -24 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad\\qquad$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrrrrr}\n",
       "-6 & -8 & 6 & -4 & 10 & 10 & 8 & -8 \\\\\n",
       "4 & 5 & -4 & 4 & -7 & -6 & -5 & 5 \\\\\n",
       "12 & 17 & -10 & 8 & -23 & -20 & -19 & 19 \\\\\n",
       "4 & 6 & -4 & 0 & -6 & -8 & -6 & 6 \\\\\n",
       "6 & 6 & -8 & 8 & -8 & -8 & -4 & 4 \\\\\n",
       "-12 & -19 & 8 & -4 & 25 & 22 & 23 & -23 \\\\\n",
       "-6 & -8 & 6 & -4 & 10 & 10 & 8 & -8 \\\\\n",
       "-24 & -28 & 28 & -24 & 36 & 36 & 24 & -24 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "M = 8; k=3\n",
    "A, A_inv = gen_inv_pb(M, maxint=3)\n",
    "_,U = gen_gj_matrix(M,k,k;maxint=2)\n",
    "_,V = gen_gj_matrix(M,k,k;maxint=2)\n",
    "printstyled( \"A and A⁻¹\", color=:blue, bold=true)\n",
    "display(l_show(L\"\\qquad A=\", A, L\"\\qquad A^{-1} =\", A_inv))\n",
    "println()\n",
    "printstyled( \"rank $k Update\", color=:blue, bold=true)\n",
    "l_show( L\"\\qquad\\qquad\", U*V')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "398f595c-1087-42ba-819a-e4f2fe8e0c0d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mInverse of the k×k matrix\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad S=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-2261 & -1676 & 1902 \\\\\n",
       "944 & 1209 & -892 \\\\\n",
       "-2294 & -1434 & 1887 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad S^{-1} = $$\\frac{1}{9739975}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-1002255 & -435144 & 804526 \\\\\n",
       "-264920 & -96681 & 221324 \\\\\n",
       "-1419750 & -602470 & 1151405 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad S=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-2261 & -1676 & 1902 \\\\\n",
       "944 & 1209 & -892 \\\\\n",
       "-2294 & -1434 & 1887 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad S^{-1} = $$\\frac{1}{9739975}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-1002255 & -435144 & 804526 \\\\\n",
       "-264920 & -96681 & 221324 \\\\\n",
       "-1419750 & -602470 & 1151405 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "S = I + V' * A_inv * U; S_inv = inv( S//1)\n",
    "dS,intS_inv=factor_out_denominator(S_inv)\n",
    "printstyled( \"Inverse of the k×k matrix\", color=:blue, bold=true)\n",
    "l_show(L\"\\qquad S=\", S, L\"\\qquad S^{-1} = \", 1//dS, intS_inv)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "d58cb5e0-d787-4277-b1c4-1c528be93586",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "inv(Rational{Int}.(A + U * V')) == A_inv - A_inv * U * S_inv * V' * A_inv = true\n"
     ]
    }
   ],
   "source": [
    "@show inv(Rational{Int}.(A + U*V')) == (A_inv-A_inv*U*S_inv*V'*A_inv);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7cedeec-ca64-4ede-bd47-e474f72f9cf9",
   "metadata": {},
   "source": [
    "___\n",
    "Note the matrix update $U V^t$ need not have full column rank $k$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "e7bc1b2a-8cc6-4df0-915f-c7851ea1f347",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mrank 1 Update\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad U V^{t} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrrrrr}\n",
       "-1 & 2 & -1 & 0 & -1 & -1 & 0 & 0 \\\\\n",
       "1 & -2 & 1 & 0 & 1 & 1 & 0 & 0 \\\\\n",
       "-1 & 2 & -1 & 0 & -1 & -1 & 0 & 0 \\\\\n",
       "-2 & 4 & -2 & 0 & -2 & -2 & 0 & 0 \\\\\n",
       "0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\\n",
       "1 & -2 & 1 & 0 & 1 & 1 & 0 & 0 \\\\\n",
       "1 & -2 & 1 & 0 & 1 & 1 & 0 & 0 \\\\\n",
       "0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad U V^{t} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrrrrrr}\n",
       "-1 & 2 & -1 & 0 & -1 & -1 & 0 & 0 \\\\\n",
       "1 & -2 & 1 & 0 & 1 & 1 & 0 & 0 \\\\\n",
       "-1 & 2 & -1 & 0 & -1 & -1 & 0 & 0 \\\\\n",
       "-2 & 4 & -2 & 0 & -2 & -2 & 0 & 0 \\\\\n",
       "0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\\n",
       "1 & -2 & 1 & 0 & 1 & 1 & 0 & 0 \\\\\n",
       "1 & -2 & 1 & 0 & 1 & 1 & 0 & 0 \\\\\n",
       "0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mInverse of the k×k matrix\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad S=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "7545 & -3772 & -3772 \\\\\n",
       "3772 & -1885 & -1886 \\\\\n",
       "7544 & -3772 & -3771 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad S^{-1} = $$\\frac{1}{1887}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-5657 & 3772 & 3772 \\\\\n",
       "-3772 & 3773 & 1886 \\\\\n",
       "-7544 & 3772 & 5659 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad S=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "7545 & -3772 & -3772 \\\\\n",
       "3772 & -1885 & -1886 \\\\\n",
       "7544 & -3772 & -3771 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad S^{-1} = $$\\frac{1}{1887}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-5657 & 3772 & 3772 \\\\\n",
       "-3772 & 3773 & 1886 \\\\\n",
       "-7544 & 3772 & 5659 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "inv(A + U * V' // 1) == A_inv - A_inv * U * S_inv * V' * A_inv = true\n"
     ]
    }
   ],
   "source": [
    "_,U = gen_gj_matrix(M,k,1;maxint=2)\n",
    "_,V = gen_gj_matrix(M,k,1;maxint=2)\n",
    "printstyled( \"rank 1 Update\", color=:blue, bold=true)\n",
    "display(l_show(L\"\\qquad U V^{t} =\",  U*V'))\n",
    "S = I + V' * A_inv * U; S_inv = inv( S//1)\n",
    "dS,intS_inv=factor_out_denominator(S_inv)\n",
    "printstyled( \"Inverse of the k×k matrix\", color=:blue, bold=true)\n",
    "display(l_show(L\"\\qquad S=\", S, L\"\\qquad S^{-1} = \", 1//dS, intS_inv))\n",
    "@show inv(A + U*V'//1) == (A_inv-A_inv*U*S_inv*V'*A_inv);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44f68c4b-d262-4e98-b657-e5eb36bd050b",
   "metadata": {},
   "source": [
    "## 1.4. Application: Solve $B x = b$ by Solving a Simpler System"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fdaaf1e-7212-40b6-88da-c7624a7b7194",
   "metadata": {},
   "source": [
    "Suppose we wish to solve $B x = b$, where $B = A + U V^t$ is close to some \"nice\" matrix $A$.\n",
    "\n",
    "We can solve $B x = b$ as follows:<br>\n",
    "$\\qquad \\begin{align}x = (A + U V^t)^{-1}\\ b\n",
    "& = A^{-1} b - A^{-1} U \\left( I + V^t A^{-1} U \\right)^{-1} V^t A^{-1} b \\\\\n",
    "& = \\tilde{x} - A^{-1} U \\left( I + V^t A^{-1} U \\right)^{-1} V^t \\tilde{x} \\\\\n",
    "\\end{align}$<br>\n",
    "$\\qquad$ where $\\tilde{x}$ is the solution to the \"simpler\" system $A \\tilde{x} = b$.\n",
    "\n",
    "$\\qquad$ As usual, we will avoid computing the inverses explicitely."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b07f2b0-d3c6-4529-9d0f-f634a831578e",
   "metadata": {},
   "source": [
    "* Step 1: Solve $A \\tilde{x} = b$\n",
    "* Step 2: Compute $W = A^{-1} U$ by solving $A W = U$\n",
    "* Step 3: Compute $y = (I + V^t W)^{-1} V^t \\tilde{x}$ by solving $(I+V^t W) y = V^t \\tilde{x}$\n",
    "* Step 4: Compute the solution  $x = \\tilde{x} - W y$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "67fc6805-4c4e-48de-9cea-86f7bb167d35",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mProblem B x = b and simpler problem A x = b, where B, A, b =\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad B=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrr}\n",
       "1 & 4 & 2 & -6 \\\\\n",
       "2 & 2 & 2 & -6 \\\\\n",
       "5 & 4 & 5 & -9 \\\\\n",
       "-2 & -3 & -2 & 6 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad A =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrr}\n",
       "-1 & 0 & 0 & 0 \\\\\n",
       "0 & -2 & 0 & 0 \\\\\n",
       "2 & -2 & 2 & 0 \\\\\n",
       "-1 & -1 & -1 & 3 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad b=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{r}\n",
       "-5 \\\\\n",
       "-2 \\\\\n",
       "2 \\\\\n",
       "3 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad B=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrr}\n",
       "1 & 4 & 2 & -6 \\\\\n",
       "2 & 2 & 2 & -6 \\\\\n",
       "5 & 4 & 5 & -9 \\\\\n",
       "-2 & -3 & -2 & 6 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad A =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrrr}\n",
       "-1 & 0 & 0 & 0 \\\\\n",
       "0 & -2 & 0 & 0 \\\\\n",
       "2 & -2 & 2 & 0 \\\\\n",
       "-1 & -1 & -1 & 3 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad b=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{r}\n",
       "-5 \\\\\n",
       "-2 \\\\\n",
       "2 \\\\\n",
       "3 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mSolution of B x = b from the solution of A x̃ = b\u001b[22m\u001b[39m"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad x =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{r}\n",
       "1 \\\\\n",
       "-1 \\\\\n",
       "2 \\\\\n",
       "1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad \\tilde{x} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{r}\n",
       "5 \\\\\n",
       "1 \\\\\n",
       "-3 \\\\\n",
       "2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad x =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{r}\n",
       "1 \\\\\n",
       "-1 \\\\\n",
       "2 \\\\\n",
       "1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad \\tilde{x} =$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{r}\n",
       "5 \\\\\n",
       "1 \\\\\n",
       "-3 \\\\\n",
       "2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# create a problem\n",
    "M = 4\n",
    "A = [-1 0 0 0; 0 -2 0 0; 2 -2 2 0; -1 -1 -1 3]\n",
    "u = [2; 2; 3; -1]\n",
    "v = [1; 2; 1; -3]\n",
    "B = A+u*v'\n",
    "b = [-5; -2; 2; 3]\n",
    "printstyled( \"Problem B x = b and simpler problem A x = b, where B, A, b =\", color=:blue, bold=true)\n",
    "display(l_show(L\"\\qquad B=\", B, L\"\\quad A =\", A, L\"\\quad b=\", b))\n",
    "\n",
    "# step 1: Solve A x̃ = b\n",
    "x̃ = A \\ b\n",
    "# step 2:  Compute W = A^{-1} U by solving A w_i = u_i, where w_i, and u_i, denote the ith column of W and U, respectively.\n",
    "w = A \\ u\n",
    "# step 3: Solve (I+VᵗW) y = Vᵗx̃\n",
    "y = (I + v'w) \\ (v'x̃)\n",
    "# step 4: Solve $x = x̃ - W y\n",
    "x = x̃ - w*y\n",
    "printstyled( \"Solution of B x = b from the solution of A x̃ = b\", color=:blue, bold=true)\n",
    "l_show( L\"\\qquad x =\", Int.(round.(x)), L\"\\quad \\tilde{x} =\", Int.(round.(x̃)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "7c669c23-0dba-4cfc-895a-f5834f1a8fc5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A * x̃ ≈ b && B * x ≈ b = true\n"
     ]
    }
   ],
   "source": [
    "#check\n",
    "@show (A*x̃ ≈ b) && (B*x ≈ b);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19686e64-b503-4243-abb1-a02ba167ceff",
   "metadata": {},
   "source": [
    "## 1.5 Application: Updating a Regression Solution"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69290df8-5e77-4855-8395-d84130b7727e",
   "metadata": {},
   "source": [
    "Consider the regression problem $argmin_x \\Vert A x - b \\Vert$<br>\n",
    "$\\qquad$ which is solved by the normal equation $A^t A x = A^t b$.\n",
    "\n",
    "We wish to update this solution when we get one or more new measurements $r, \\tilde{b}$, i.e.,<br>\n",
    "$\\qquad$ we now want the solution of\n",
    "$\\begin{pmatrix} A^t & r^t \\end{pmatrix} \\begin{pmatrix} A \\\\ r \\end{pmatrix} \\tilde{x}\n",
    "= \\begin{pmatrix} A^t & r^t \\end{pmatrix} \\begin{pmatrix} b \\\\ \\tilde{b} \\end{pmatrix}\\;\\;\\Leftrightarrow \\;\\;\n",
    "(A^t A + r^t r) \\tilde{x} =A^t b + r^t\\tilde{b}$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1b8d7c3-8ba7-4358-b3b4-7480c9e4d9be",
   "metadata": {},
   "source": [
    "Setting $D = I,\\; A = B,\\; U=r^t$ and $V=r$ in Eq 3, we obtain\n",
    "\n",
    "$\\qquad (B + r^t r)^{-1} = \\left( I - W \\left( I + r W \\right)^{-1} r \\right) B^{-1},\\;\\;$ where $\\;\\;W=B^{-1} r^t,\\;$\n",
    "and $B = A^t A$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "905bb004-bc3d-48e2-a9ff-2bd986cc24fd",
   "metadata": {},
   "source": [
    "Applying this equation to the right hand side $A^t b + r^t \\tilde{b}$, the updated solution $\\tilde{x}$ of the normal equation is given by<br>\n",
    "$\\qquad\\tilde{x} = \\left( I - W \\left( I + r W \\right)^{-1} r \\right) \\left( x + W \\tilde{b} \\right)$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdd45b46-0764-4654-9579-b5c25799fefd",
   "metadata": {},
   "source": [
    "In the case where $r$ is size $1 \\times N$, i.e., a single measurement, this reduces to<br>\n",
    "$\\qquad \\tilde{x} = \\left( I - \\frac{1}{\\gamma} W r \\right) \\left( x + W\\tilde{b}\\right)$, where \n",
    "$\\gamma = 1 + r W$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a107e14-beb9-44d8-9953-bdcab4d8c288",
   "metadata": {},
   "source": [
    "#### Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "ff7bdd4a-ee7f-4670-a945-3a3dcfe5e87a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mEstimate:     y = 1.382 + 3.034 x\u001b[22m\u001b[39m\n",
      "\u001b[34m\u001b[1mNew Estimate: y = 1.36 + 3.036 x\u001b[22m\u001b[39m"
     ]
    }
   ],
   "source": [
    "# Set up a problem AᵗA x = Aᵗ b and an update\n",
    "model(x) = 2 .+ 3x\n",
    "M = 20; k = 5\n",
    "Ã = [ones(M+k) 1:(M+k)]\n",
    "A = Ã[1:M,1:end]\n",
    "r = Ã[M+1:end,1:end]\n",
    "b = model(A[:,2])+randn(M)\n",
    "b̃ = model(r[:,2])+randn(k)\n",
    "\n",
    "# Solve the normal equation and the updated normal equation\n",
    "x     = (A'A) \\ (A'b)\n",
    "x_new = (Ã'Ã) \\ (Ã'*[b;b̃])\n",
    "\n",
    "printstyled(\"Estimate:     y = $(round(x[1],digits=3)) + $(round(x[2],digits=3)) x\\n\", color=:blue, bold=true)\n",
    "printstyled(\"New Estimate: y = $(round(x_new[1],digits=3)) + $(round(x_new[2],digits=3)) x\", color=:blue, bold=true)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "6211346c-fdfd-46f2-a091-85f1bd8b5496",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[34m\u001b[1mNew Estimate: y = 1.36 + 3.036 x\u001b[22m\u001b[39m"
     ]
    }
   ],
   "source": [
    "# update the previous solution instead:\n",
    "B = A'A                         # the original Gram Matrix\n",
    "W = B \\ r'\n",
    "y = (I+r*W) \\ r\n",
    "x̃ = (I-W*y)*(x+ W*b̃)            # the updated solution\n",
    "printstyled(\"New Estimate: y = $(round(x̃[1],digits=3)) + $(round(x̃[2],digits=3)) x\", color=:blue, bold=true)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5608f75b-b02c-4d80-883a-4de1ddc47615",
   "metadata": {},
   "source": [
    "# 2. Other Useful Formulae"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "122e5563-27ab-476f-a47f-d5c2aee11c98",
   "metadata": {},
   "source": [
    "The following formulae commute terms $A B$ to terms $B A$. These equalities follow from $B ( A B ) = ( B A ) B$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c33dfdc7-cb4a-47ca-b8a7-cdab47413193",
   "metadata": {},
   "source": [
    "<div style=\"float:left;padding-left:0.8cm;;\">\n",
    "\\begin{aligned}\n",
    "& B \\left( I_m + A B \\right)  & = &\\quad \\left(I_n + B A \\right) B \\\\\n",
    "& B \\left( I_m + A B \\right)^{-1} & = &\\quad \\left(I_n + B A \\right)^{-1} B \\\\\n",
    "& A^t \\left( A A^t + \\lambda I_n \\right)^{-1} & = &\\quad \\left(A^t A + \\lambda I_m \\right)^{-1} A^t \\\\\n",
    "& U \\left( I_k + V^t U \\right) & = &\\quad \\left(I_n + U V^t \\right) U \\\\\n",
    "\\end{aligned}\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6864174d-74c8-4157-83a9-e1c5e087a1e7",
   "metadata": {},
   "source": [
    "We also have\n",
    "\n",
    "$\\qquad B^{-1} - A^{-1} = B^{-1} ( A - B ) A^{-1}$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.11.1",
   "language": "julia",
   "name": "julia-1.11"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
