{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "using Pkg, Revise\n",
    "Pkg.activate(\"../GenLinAlgProblems\")\n",
    "using GenLinAlgProblems, AbstractAlgebra, LinearAlgebra, Polynomials, LaTeXStrings, Latexify, SymPy\n",
    "AbstractAlgebra.charpoly(A::Matrix) = charpoly(ZZ[\"x\"][1], matrix(ZZ,A))\n",
    "Base.showable(::MIME\"text/html\", ::Polynomial) = false"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div style=\"height:2cm;\">\n",
    "<div style=\"float:center;width:100%;text-align:center;\"><strong style=\"height:100px;color:darkred;font-size:40px;\">The Hamilton Cayley Theorem</strong>\n",
    "</div></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. The Hamilton Cayley Theorem and the Inverse of a Matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Preliminary: Matrix Polynomials"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the polynomial $p(\\lambda) = \\lambda^2 + 5 \\lambda - 12$\n",
    "and a square matrix $A$\n",
    "\n",
    "Since powers of a square matrix have the same size, we can readily compute the first two terms in the polynomial:\n",
    "$A^2 + 5 A$, but we can't add the scalar $-2$ to this sum. If we extend the definition $\\lambda^0 = 1$ to matrices,\n",
    "i.e., $A^0 = I$, we can rewrite\n",
    "\n",
    "$\\qquad\n",
    "p(\\lambda) = \\lambda^2 + 5 \\lambda^1 -12 \\lambda^0\n",
    "$\n",
    "\n",
    "We can now formally define the function\n",
    "\n",
    "$\\qquad\n",
    "p(A) = A^2 + 5 A^1 + 6 A^0  = A^2 + 5 A -12 I\n",
    "$\n",
    "\n",
    "We can generalize this idea for any polynomial and any square matrix A:\n",
    "\n",
    "$\\qquad\n",
    "\\text{Given  } p(\\lambda) = \\sum_{n=0}^N \\alpha_n \\lambda^n, \\text{   define }\n",
    "               p(A) = \\sum_{n=0}^N \\alpha_n A^n\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\qquad A=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "0 & -1 & 1 \\\\\n",
       "1 & 2 & -1 \\\\\n",
       "-1 & -1 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad p(A) = A^2 + 5 A -12 I = $$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-14 & -8 & 8 \\\\\n",
       "8 & 2 & -8 \\\\\n",
       "-8 & -8 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad A=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "0 & -1 & 1 \\\\\n",
       "1 & 2 & -1 \\\\\n",
       "-1 & -1 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\qquad p(A) = A^2 + 5 A -12 I = $$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "-14 & -8 & 8 \\\\\n",
       "8 & 2 & -8 \\\\\n",
       "-8 & -8 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A = Rational.([0 -1 1;  1 2 -1; -1 -1 2])\n",
    "l_show( L\"\\qquad A=\", A, L\"\\qquad p(A) = A^2 + 5 A -12 I = \", (A*A+5*A-12*I))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 The Cayley Hamilton Theorem\n",
    "\n",
    "**Let $A$ be a square matrix with characteristic polynomial $p(\\lambda)$, the $p(A) = 0$.**\n",
    "\n",
    "The characteristic polynomial for the example matrix is $p(\\lambda) = -(\\lambda - 2) (\\lambda - 1)^2$.\n",
    "\n",
    "Let's check:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/latex": [
       "$\\qquad\\text{The characteristic polynomial of A is given by }$$\\qquad p(\\lambda) = -λ^3 + 4*λ^2 - 5*λ + 2$"
      ],
      "text/plain": [
       "L\"$\\qquad\\text{The characteristic polynomial of A is given by }$$\\qquad p(\\lambda) = -λ^3 + 4*λ^2 - 5*λ + 2$\""
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l_show( L\"\\qquad\\text{The characteristic polynomial of A is given by }\", L\"\\qquad p(\\lambda) = %$(charpoly(A))\" )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The roots of the characteristic polynomial are its eigenvalues. They are given by  [1 1 2]\n"
     ]
    }
   ],
   "source": [
    "print( \"The roots of the characteristic polynomial are its eigenvalues. They are given by  \")\n",
    "println(Int.(round.(eigvals(A), digits=3))' );"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A satisfies the characteristic polynomial\n",
      "((A ^ 3 - 4 * A ^ 2) + 5A) - 2I == zeros(Int, 3, 3) = true\n"
     ]
    }
   ],
   "source": [
    "println(\"A satisfies the characteristic polynomial\")\n",
    "@show (A^3 - 4A^2 + 5A - 2I) == zeros( Int, 3,3);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.4 Application: a Formula for the Inverse\n",
    "\n",
    "We know that $p(\\lambda) = det(A-\\lambda I)$.\n",
    "\n",
    "Substituting $\\lambda=0$, we see that the\n",
    "constant term in the characteristic polynomial is $p(0) = det(A)$.\n",
    "\n",
    "We also know that a matrix $A$ is invertible iff $det(A) \\ne 0$.\n",
    "We can therefore solve $p(A)= 0$ for this constant term $det(A) I$:\n",
    "\n",
    "$\\qquad\n",
    "p(A) = \\sum_{n=0}^N \\alpha_n A^n \\Leftrightarrow det(A) I = - \\sum_{n=1}^N \\alpha_n A^n\n",
    "$\n",
    "\n",
    "The term on theright hand side has a common factor $A$. For invertible matrices, we see that\n",
    "\n",
    "$\\qquad\n",
    "I = A \\left( \\frac{-1}{\\text{det}\\,A} \\sum_{n=1}^{N} { \\alpha_n A^{n-1} } \\right) \n",
    "$\n",
    "\n",
    "The term in parntheses must therefore be the inverse of $A$!\n",
    "\n",
    "Let's check our example:\n",
    "\n",
    "$\\qquad\n",
    "p(\\lambda) = -\\lambda ( \\lambda^2 - 4 \\lambda + 5 ) + 2\n",
    "$\n",
    "\n",
    "so\n",
    "\n",
    "$\\qquad\n",
    "A^{-1} = \\frac{1}{2} \\left( A^2 - 4 A + 5 I \\right)\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### 1.4.1 General 2x2 matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\qquad A = \\begin{pmatrix} a & b \\\\ c & d \\end{pmatrix} \\qquad \\Rightarrow  \\quad\n",
    "p(\\lambda) = ( a d - b c ) - ( a + d ) \\lambda  + \\lambda^{2} \\qquad \\Rightarrow  \\quad\n",
    "(a d - b c) I = \\left( (a + d ) I - A \\right) A\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\therefore \\quad A^{-1} = \\frac{1}{a d - b c} \\left( (a+b) I - A \\right) =  \\frac{1}{a d - b c} \\begin{pmatrix} d & -b \\\\ -c & a \\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using Cayley Hamilton to compute the inverse of a 2x2 matrix\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\left[\\begin{smallmatrix}a & b\\\\c & d\\end{smallmatrix}\\right]$"
      ],
      "text/plain": [
       "2×2 Matrix{Sym{PyCall.PyObject}}:\n",
       " a  b\n",
       " c  d"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p(λ) = a*d - b*c + λ^2 + λ*(-a - d)\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$M^{-1}=$$\\frac{1}{a \\cdot d - b \\cdot c}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rr}\n",
       "d &  - b \\\\\n",
       " - c & a \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$M^{-1}=$$\\frac{1}{a \\cdot d - b \\cdot c}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rr}\n",
       "d &  - b \\\\\n",
       " - c & a \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(a,b,c,d)=SymPy.symbols(\"a,b,c,d\", real=true)\n",
    "λ = SymPy.symbols(\"λ\")\n",
    "\n",
    "M = [a b; c d]\n",
    "println(\"Using Cayley Hamilton to compute the inverse of a 2x2 matrix\")\n",
    "display(M)\n",
    "p(l) = SymPy.factor(det(M-l*I), λ)\n",
    "@show p(λ)\n",
    "l_show(L\"M^{-1}=\", 1/det(M), (-M+(a+d)*I))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### 1.4.2 Continuation of the 3x3 Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Inverse of the matrix A using the characteristic polynomial\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad A^{-1}=$$\\frac{1}{2}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "3 & 1 & -1 \\\\\n",
       "-1 & 1 & 1 \\\\\n",
       "1 & 1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad A^{-1}=$$\\frac{1}{2}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "3 & 1 & -1 \\\\\n",
       "-1 & 1 & 1 \\\\\n",
       "1 & 1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"Inverse of the matrix A using the characteristic polynomial\")\n",
    "l_show(L\"\\qquad A^{-1}=\", 1//2, A^2-4A+5I )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A⁻¹ using the builtin function \n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad A^{-1}=$$\\frac{1}{2}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "3 & 1 & -1 \\\\\n",
       "-1 & 1 & 1 \\\\\n",
       "1 & 1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad A^{-1}=$$\\frac{1}{2}$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "3 & 1 & -1 \\\\\n",
       "-1 & 1 & 1 \\\\\n",
       "1 & 1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println( \"A⁻¹ using the builtin function \")\n",
    "l_show(L\"\\qquad A^{-1}=\", 1//2, 2*inv(A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Note\n",
    "Here is how I set up a nice matrix $A$\n",
    "\n",
    "\n",
    "I know that a complete set of eigenvectors yields a decomposition\n",
    "$A = S \\Lambda S^{-1}$,<br>\n",
    "where $\\Lambda$ is a diagonal matrix (the eigenvalues are on the diagonal).\n",
    "\n",
    "So create a full matrix $S$ with determinant equal to 1 so $S^{-1}$ is nice,\n",
    "and multiply out such an expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Example generation of a nice eigenproblem\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "$\\qquad S=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "1 & -1 & 1 \\\\\n",
       "-1 & 2 & -1 \\\\\n",
       "0 & 1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad D=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "1 & 0 & 0 \\\\\n",
       "0 & 1 & 0 \\\\\n",
       "0 & 0 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad A=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "0 & -1 & 1 \\\\\n",
       "1 & 2 & -1 \\\\\n",
       "-1 & -1 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$"
      ],
      "text/plain": [
       "L\"$\\qquad S=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "1 & -1 & 1 \\\\\n",
       "-1 & 2 & -1 \\\\\n",
       "0 & 1 & 1 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad D=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "1 & 0 & 0 \\\\\n",
       "0 & 1 & 0 \\\\\n",
       "0 & 0 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$$\\quad A=$$\\begin{equation}\n",
       "\\left(\n",
       "\\begin{array}{rrr}\n",
       "0 & -1 & 1 \\\\\n",
       "1 & 2 & -1 \\\\\n",
       "-1 & -1 & 2 \\\\\n",
       "\\end{array}\n",
       "\\right)\n",
       "\\end{equation}\n",
       "$\""
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "println(\"Example generation of a nice eigenproblem\")\n",
    "S = Rational.( [1 0 0; -1 1 0; 0 1 1]\n",
    "             * [1 0 0; -1 1 0; 1 0 1]' )\n",
    "D = Diagonal([1,1,2])\n",
    "A = S * D * inv(S)\n",
    "l_show(L\"\\qquad S=\", S, L\"\\quad D=\", D, L\"\\quad A=\", A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a latex version of the matrix ready for text display is given by\n",
    "# println(latexify(A))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.11.1",
   "language": "julia",
   "name": "julia-1.11"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
